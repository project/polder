********************************************************************
                      D R U P A L    T H E M E                         
********************************************************************
Name: Polder 
Version: 3.0
Author: Robert Laarhoven 
Email: robert at polder.net 
Last update: 9 Nov. 2003 
Drupal: 4.3
Style module support

********************************************************************
DESCRIPTION:
 
Super de Luxe and 'ass kicking' Drupal theme. Default style 
(colors and fonts) can be changed easily in Drupal's admin 
section when the style module is enabled.

Colors and typography controlled by stylesheet, positions 
set through tables. XHTML 1.0 compatible. Scalable layout, 
fixed side bar width, low in graphics. No round edges.

Support for blocks on the left and/or right side. By default 
only the left side blocks are shown. 

Supports for taxonomy, taxonomy icons, and avatars. 

Under MS Windows tested and compatible with Mozilla 1.x, 
Opera 6.0.1 and Internet Explorer 4.0, 5.0 and 6.0. 

Under Linux tested and compatible with Mozilla 1.x and 
Konqueror 2.2.1 and 2.2.2.

Not tested on Mac Internet Explorer and Mac Netscape (yet).

********************************************************************
INSTALLATION: 

Copy the polder/ theme directory into your drupal themes/ 
directory. Enable the theme in your administration settings. 

If you want to tweak the colors and some other things easily in 
the admin section, install and enable the style module. Or you 
can change some vars in the polder.inc file in the Polder theme 
directory.

If you want to use a logo in the headers, replace themes/polder/images/logo.gif with your own logo, 
and if needed replace drop.gif. Enable the logo in the 
style admin 'expert mode', or the polder. inc file. By default  
a header text is used, which you also can change easily.

If you want to use icons for taxonomy terms, add icons named 
'term.gif' (use lowercase and underscores for term) to the 
directory themes/polder/images/icons/ and enable icons support 
in the theme admin settings. 

Avatars are disabled by default and can be enabled in the 
'expert' mode.

To support left side blocks, you first have to set the column 
width in the theme administration settings. You can disable or 
hide the right column by setting its width to zero.

See the help page in the site administration section for further 
instructions on how to tweak your pages using the style module.

********************************************************************

